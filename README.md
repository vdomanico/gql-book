Example of graphql query -> http://localhost:8080/graphql

{
  bookById(id: "1"){
    id
    name
    pageCount
    author {
      firstName
      lastName
    }
  }
}