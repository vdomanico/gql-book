package jbelair.book;

import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;
import jbelair.book.service.GraphQLDataFetchersService;
import org.apache.commons.io.IOUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.io.InputStream;

import static graphql.schema.idl.TypeRuntimeWiring.newTypeWiring;

@SpringBootApplication
public class App {

    public static void main(String[] args){
        SpringApplication.run(App.class, args);
    }

    @Bean
    public GraphQL graphQL(GraphQLDataFetchersService graphQLDataFetchersService) throws IOException {
        try(InputStream iS = new ClassPathResource("schema.graphql").getInputStream()) {
            String sdl = IOUtils.toString(iS);
            return GraphQL.newGraphQL(buildSchema(sdl, graphQLDataFetchersService)).build();
        }
    }

    private GraphQLSchema buildSchema(String sdl, GraphQLDataFetchersService graphQLDataFetchersService) {
        TypeDefinitionRegistry typeRegistry = new SchemaParser().parse(sdl);
        RuntimeWiring runtimeWiring = buildWiring(graphQLDataFetchersService);
        SchemaGenerator schemaGenerator = new SchemaGenerator();
        return schemaGenerator.makeExecutableSchema(typeRegistry, runtimeWiring);
    }

    private RuntimeWiring buildWiring(GraphQLDataFetchersService service) {
        return RuntimeWiring.newRuntimeWiring()
                .type(newTypeWiring("Query").dataFetcher("bookById", service.getBookByIdDataFetcher()))
                .build();
    }



}
