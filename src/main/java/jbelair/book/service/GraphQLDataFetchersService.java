package jbelair.book.service;

import graphql.schema.DataFetcher;
import jbelair.book.model.Author;
import jbelair.book.model.Book;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GraphQLDataFetchersService {

    private static final Author author1 = new Author("1", "Mario", "Rossi");
    private static final Book book1 = new Book("1", "ABC", 100, author1);
    private static final Book book2 = new Book("2", "XYZ", 50, author1);
    private static final List<Book> books = List.of(book1, book2);

    public DataFetcher<Book> getBookByIdDataFetcher() {
        return dataFetchingEnvironment -> books.stream()
                .filter(b -> b.getId().equals(dataFetchingEnvironment.getArgument("id")))
                .findFirst().orElse(null);
    }

}
